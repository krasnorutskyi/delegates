using System;

namespace Delegates
{
    public delegate void CreateUserId<T>(T id);

    public class GenericClass<T>
    {
        private CreateUserId<T> userId;

        public GenericClass(CreateUserId<T> id)
        {
            userId = id;
        }

        public void Execute(T param)
        {
            userId.Invoke(param);
        }
    }
}
