﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Delegates
{
    internal class Program
    {
        private delegate void FirstDelegate<T>(T param);

        private delegate TOut SecondDelegate<T, TOut>(T first, TOut second);

        private delegate int Calculate(int a, int b);

        private delegate bool GoThroughCollection(int a);

        private delegate int SelectFromTheCollection(int a);

        static void Main(string[] args)
        {
            List<int> Numbers = new List<int>() { 2, 35, 4, 13, 6, 8, 9 };
            GoThroughCollection go;
            go = IsEven;
            SelectFromTheCollection select;
            select = MultiplyByThree;

            FirstDelegate<string> first;
            first = Greeting;
            first("Tom");

            SecondDelegate<int, double> second;
            second = Multiply;
            Console.WriteLine(second(3, 1.33));

            Calculate calc = (a, b) => (a + b) / ((a + b) / 2);
            Console.WriteLine(calc(4, 5));

            var processedCollection = ProcessCollection(go, select, Numbers);
            foreach (var element in processedCollection)
            {
                Console.WriteLine(element);
            }
        }

        static int MultiplyByThree(int a)
        {
            var b = a * 3;
            return b;
        }

        static bool IsEven(int a)
        {
            if (a % 2 == 0)
            {
                return true;
            }

            return false;
        }

        static double Multiply(int a, double b)
        {
            return a * b;
        }

        static void Greeting(string name)
        {
            Console.WriteLine($"Hello, {name}!");
        }

        static List<int> ProcessCollection(GoThroughCollection go, SelectFromTheCollection select, List<int> list)
        {
            var processedList = list.Select(x => select(x)).Where(x => go(x)).ToList();
            return processedList;
        }
    }
}